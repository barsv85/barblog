import os, sys, datetime

# path to the root folder of the blog.
# E.g. if your blog is at yourDomain.com/blog then correct value is '/blog'
# if your blog is at yourDomain.com then correct value is ''
BLOG_ROOT = '/BarBlog'
# name of the folder with blog posts
POSTS_DIR = 'posts'
# extension of the template files
TEMPLATE_EXT = '.template'
# extension of the markdown files
MD_EXT = '.md'
# markdown files that are ignored during transformations to html
IGNORED_MD = ['readme.md']
# prefix for tags
TAGS_PREFIX = 'tags'

def get_title(filename):
    """get title from filename"""
    if filename.endswith(MD_EXT):
        filename = filename[:-3]
    firstPoint = find_in_iterable('.', filename)
    if (firstPoint > 0):
        filename = filename[firstPoint + 1:]        
    return filename

def get_title_from_path(path):
    """Get title form full path"""
    fileName = os.path.basename(path)
    return get_title(fileName)    
    
def find_in_iterable(x, iterable):
    """Finds an element x in a collection iterable"""
    for i, item in enumerate(iterable):
        if item == x:
            return i
    return -1

def get_creation_date(filename):
    """Returns file creation timeStamp"""
    t = os.path.getctime(filename)
    return datetime.datetime.fromtimestamp(t)

def title_to_dirName(title):
    """Gets folderName for a title.
    Each *.md file is transformed into index.html
    that is placed in a separate folder
    with name formed from *.md file name.
    All characters that can't be in a folderName or URL
    are replaced with underscore.
    """
    return title.replace(' ', '_')

def find_template(dirPath, title):
    """Finds a *.template file for given title.
    The convension is the following:
    For markdown files: (prefix.some name.md)
    where:
     (prefix) is an arbitrary optional prefix. it is used only to sort files.
     (some name) is a tilte of the future html page.
     (md) is markdown file extenstion.
    For templates: (some name.template) or (index.template)
    where:
     (some name) must match title of the corresponding markdown file.
     if there are no file named (some name.template) then program looks
     for a file named index.template and uses it as a template.
    """
    for sub in os.listdir(dirPath):
        subFullPath = os.path.join(dirPath, sub)
        if (subFullPath.endswith(title + TEMPLATE_EXT)):
            return subFullPath
    subFullPath = os.path.join(dirPath, 'index' + TEMPLATE_EXT)
    if (os.path.exists(subFullPath)):
        return subFullPath
    raise Exception("Can't find template " + subFullPath)

def get_md_file_folder(path):
    """Returns folder path for a given md file path."""
    fileName = os.path.basename(path)
    title = get_title(fileName)
    dirName = os.path.dirname(path)
    folderName = title_to_dirName(title)
    folder = os.path.join('.', dirName, folderName)
    return folder

def get_md_file_url(path):
    """Returns relative url for a given md file path."""
    fileName = os.path.basename(path)
    dirName = os.path.dirname(path)
    title = get_title(fileName)
    dirUrl = dirName[1:].replace('\\','/')
    titleUrl = title_to_dirName(title)
    url = BLOG_ROOT + dirUrl + "/" + titleUrl
    return url
