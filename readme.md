## BarBlog
Static markdown blog engine. Writen in python.

Running example [http://baranov.me/BarBlog/](http://baranov.me/BarBlog/)

## Requirements:

 - [Markdown](https://pypi.python.org/pypi/Markdown)
 
## How to use

Put your posts into the folder "posts".
Run generate.py

## Settings

A bit of configuration can be done by setting 
constants at the top of common.py.